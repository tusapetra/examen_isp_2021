package Examen;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiect2 extends JFrame {
    JTextField t1, t2, t3;
    JButton button;

    Subiect2() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setTitle("Subiect2-Tusa Petra");
        setSize(300,300);

        init();

    }

    public void init() {
        setLayout(null);
        t1 = new JTextField();
        t1.setBounds(20,20,100,20);

        t2 = new JTextField();
        t2.setBounds(20,60,100,20);

        t3 = new JTextField();
        t3.setBounds(20,100,100,20);

        button = new JButton("Suma");
        button.setBounds(20,140,100,30);
        button.addActionListener(new Action());

        add(t1); add(t2); add(t3); add(button);
    }

    public static void main(String[] args) {
        new Subiect2();
    }

    class Action implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String txt1 = Subiect2.this.t1.getText();
            String txt2 = Subiect2.this.t2.getText();
            int x = Integer.valueOf(txt1);
            int y = Integer.valueOf(txt2);
            int rezultat = x + y;
            String s = String.valueOf(rezultat);
            Subiect2.this.t3.setText(s);
        }
    }
}


